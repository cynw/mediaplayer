package com.example.cwong.mediaplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cwong on 16. 4. 24.
 */
public class FileAdapter extends ArrayAdapter<String> {
    private List<String> files;
    private List<String> paths;
    private List<String> artists;
    private List<Bitmap> covers;

    public FileAdapter(Context context, int resource, List<String> objects, List<String> paths, List<String> artists, List<Bitmap> covers) {
        super(context, resource, objects);
        files = objects;
        this.paths = paths;
        this.artists = artists;
        this.covers = covers;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.explorer_lv, null);
        }
        String f = files.get(position);

        if (f != null) {

            TextView filename_tv = (TextView) convertView.findViewById(R.id.filename_tv);
            ImageView icon_iv = (ImageView) convertView.findViewById(R.id.icon_iv);


            if (filename_tv != null){
                filename_tv.setText(f);
            }
            if (icon_iv != null) {
                icon_iv.setImageResource(R.drawable.ic_movie_black_24dp);
            }
            convertView.setTag(R.string.app_name, paths.get(position));
        }
        return convertView;
    }
}
