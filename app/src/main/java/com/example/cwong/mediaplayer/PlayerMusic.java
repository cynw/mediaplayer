package com.example.cwong.mediaplayer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

public class PlayerMusic extends Service implements MediaPlayer.OnPreparedListener{
    private NotificationManager mNM;
    private MediaPlayer mp;
    private String path;
    private String playing_path=null;

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();



    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        playing_path=path;
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        PlayerMusic getService() {
            return PlayerMusic.this;
        }
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "started", Toast.LENGTH_SHORT).show();

        // Display a notification about us starting.  We put an icon in the status bar.
        mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.setOnPreparedListener(this);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_library_music_black_24dp)
                        .setContentTitle("MediaPlayer")
                        .setContentText("Playing");
        mBuilder.setOngoing(true);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Player.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
        mNM.notify(0, mBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        path= intent.getStringExtra("path");
        if (path!=null) {
            if (playing_path==null || !path.equals(playing_path)) {
                mp.stop();
                mp.release();
                mp = null;
                mp = new MediaPlayer();
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.setOnPreparedListener(this);
                setDataSource(path);
            }
        }
        return START_NOT_STICKY;
    }

    public void start(){
        mp.start();
    }

    public void setDataSource(String f){
        try {
            mp.setDataSource(f);
            mp.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pause(){
        mp.pause();
    }

    public int getDuration(){
        return mp.getDuration();
    }

    public int getCurrentPosition(){
        return mp.getCurrentPosition();
    }

    public void seekTo(int pos){
        mp.seekTo(pos);
    }

    public boolean isPlaying() {
        return mp.isPlaying();
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        mp.stop();
        mp.release();
        mp = null;

        // Tell the user we stopped.
        Toast.makeText(this, "stopped", Toast.LENGTH_SHORT).show();
        mNM.cancel(0);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}