package com.example.cwong.mediaplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.Toast;

public class Player extends AppCompatActivity implements MediaController.MediaPlayerControl {

    private Toolbar toolbar;
    private String file;
    private MediaController mc;
    PlayerMusic mService;
    private int position;
    private Context c;
    private SeekBar sb;
    private AudioManager audioManager;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        sb = (SeekBar) findViewById(R.id.seekBar);

        setSupportActionBar(toolbar);

        c = this;

        toolbar.setNavigationIcon(R.drawable.ic_action_name);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        file = getIntent().getStringExtra("path");
        position = getIntent().getIntExtra("position", 0);

        mc = new MediaController(this);
        mc.setMediaPlayer(this);
        mc.setAnchorView((MediaController) findViewById(R.id.mediaController));
        mc.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position<MainActivity.paths.size()-1) {
                    Intent intent = new Intent(c, Player.class);
                    intent.putExtra("path", MainActivity.paths.get(position+1));
                    intent.putExtra("position", position+1);
                    startActivity(intent);
                }else
                    Toast.makeText(c, R.string.end_playlist, Toast.LENGTH_SHORT).show();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position > 0) {
                    Intent intent = new Intent(c, Player.class);
                    intent.putExtra("path", MainActivity.paths.get(position-1));
                    intent.putExtra("position", position-1);
                    startActivity(intent);
                }else
                    Toast.makeText(c, R.string.end_playlist, Toast.LENGTH_SHORT).show();
            }
        });

        Intent intent = new Intent(this, PlayerMusic.class);
        intent.putExtra("path", file);
        startService(intent);
        if (!mBound)
            bindService(intent, mConnection, Context.BIND_NOT_FOREGROUND);

        if(sb!=null){

            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            sb.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            sb.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    // TODO Auto-generated method stub
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);

                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void start() {
        if (mBound) {
            mService.start();
        }
    }


    @Override
    public void pause() {
        if (mBound)
            mService.pause();
    }

    @Override
    public int getDuration() {
        if (mBound)
            return mService.getDuration();
        else
            return 0;
    }

    @Override
    public int getCurrentPosition() {
        if (mBound)
                return mService.getCurrentPosition();
        else
            return 0;
    }

    @Override
    public void seekTo(int pos) {
        if (mBound)
            mService.seekTo(pos);
    }

    @Override
    public boolean isPlaying() {
        if (mBound)
            return mService.isPlaying();
        else
            return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mc.show(0);
        return super.onTouchEvent(event);
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            PlayerMusic.LocalBinder binder = (PlayerMusic.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

}
