**Features**
1. Show all audio files stored in the device
2. Allow user to play and control the player
   * User can pause the playing track        
   * User can seek to a certain point of the music via seekbar
   * User can play the next or previous track of music directly from the player
   * User can control the loudness of the music from the screen
3. The music keeps playing in background even the app is not in foreground
4. Tap on the notification bar to go back to the playing music
5. Only one music is playing at a time


**How to run this application**
1
![Screenshot_20160426-001124.png](https://bitbucket.org/repo/6koXzb/images/2854621969-Screenshot_20160426-001124.png)
2
![Screenshot_20160426-001503.png](https://bitbucket.org/repo/6koXzb/images/1481009072-Screenshot_20160426-001503.png)
4
![Screenshot_20160426-001948.png](https://bitbucket.org/repo/6koXzb/images/402279131-Screenshot_20160426-001948.png)
	

1. Launch the application. Press “Allow” if it asks for the permission to access the files.
2. Select the media you want to play from the list
3. You can enjoy your music. With the following screen.
![Screenshot_20160426-001848.png](https://bitbucket.org/repo/6koXzb/images/3718730810-Screenshot_20160426-001848.png) 
4. You may exit from the player screen while continue enjoying your music. Tap on the notification bar to reenter the currently playing music.


**Design & Implementation**
1. I get the media stored in the devices by an Android API, getContentResolver().query. I stored the results of the query in ArrayList.
2. I passed the stored ArrayList from (1) to FileAdapter, my custom ArrayAdapter for displaying each ListView element.
3. When an element in the ListView is clicked, forward the path of the media to Player Activity.
4. Player inflates the layout and forward the path to PlayMusic Service. Player keeps track of any input to the MediaController widget and volume control Seekbar. Player activity calls functions in PlayMusic Service when user control MediaController.
5. PlayMusic service initiate MediaPlayer, set path and receive any input from Player.


**Code**
https://bitbucket.org/cynw/mediaplayer